package com.example.philip.ass41;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends ActionBarActivity {

    Button queenButton;
    Button duckButton;
    Button grindButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        queenButton = (Button) findViewById(R.id.youtube_link_1);
        duckButton = (Button) findViewById(R.id.youtube_link_2);
        grindButton = (Button) findViewById(R.id.youtube_link_3);

        queenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openYouTube("https://www.youtube.com/watch?v=HgzGwKwLmgM");
            }
        });
        duckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openYouTube("https://www.youtube.com/watch?v=7dS9lSptc6Q");
            }
        });
        grindButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openYouTube("https://www.youtube.com/watch?v=TWfph3iNC-k");
            }
        });
    }

    private void openYouTube(String url) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }
}
